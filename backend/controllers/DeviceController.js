const DeviceService = require('../services/DeviceService')
const Util = require('../utils/Utils')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const util = new Util()

class DeviceController {
    static async get_all_devices_of_user_from_house(req, res) {
        const user = req.user
        const { idHouse } = req.params

        try {
            const devices = await DeviceService.get_all_devices_of_user_from_house(idHouse, user.idUser)
            if (devices.length > 0) {
                util.setSuccess(200, 'Devices retrieved', devices)
            } else {
                util.setError(404, 'No device found')
            }
            return util.send(res)
        } catch (error) {
            util.setError(500, `Internal error`)
            return util.send(res)
        }
    }

    static async get_device_from_house(req, res) {
        const user = req.user
        const { idHouse, idDevice } = req.params

        try {
            const device = await DeviceService.get_device_from_house(idHouse, user.idUser, idDevice)

            if (!device) {
                util.setError(404, `Cannot find device with the id ${idDevice}`)
            } else {
                util.setSuccess(200, 'Found device', device)
            }
            return util.send(res)
        } catch (error) {
            util.setError(500, `Internal error`)
            return util.send(res)
        }
    }

    static async add_device_to_house(req, res) {
        const { idDevice } = req.body
        const { idHouse } = req.params
        const user = req.user

        if (!idDevice) {
            util.setError(400, `Please provide an idDevice`)
            return util.send(res)
        }

        try {
            const newDevice = await DeviceService.add_device_to_house(parseInt(idHouse), idDevice, user.idUser)
            if (newDevice === 'house') {
                util.setError(409, `User doesn\'t exist in house`)
                return util.send(res)
            }
            if (newDevice === 'device') {
                util.setError(403, `${idDevice} already exists`)
                return util.send(res)
            }
            util.setSuccess(201, `Device ${idDevice} Added!`, newDevice)
            return util.send(res)
        } catch (error) {
            util.setError(500, `Internal error`)
            return util.send(res)
        }
    }

    static async update_device_house(req, res) {
        const { idDevice } = req.params
        const { idHouse } = req.body
        const user = req.user

        if (!idHouse) {
            util.setError(400, `Please provide an idHouse`)
            return util.send(res)
        }

        try {
            const newDevice = await DeviceService.update_device_house(parseInt(idHouse), idDevice, user.idUser)
            if (newDevice === 'oldHouse') {
                util.setError(409, `User doesn\'t have rights in old house`)
                return util.send(res)
            }
            if (newDevice === 'newHouse') {
                util.setError(409, `User doesn\'t have rights in new house`)
                return util.send(res)
            }
            if (newDevice === 'device') {
                util.setError(409, `User doesn\'t have rights to modify device`)
                return util.send(res)
            }
            util.setSuccess(200, `Device ${idDevice} changed to house ${idHouse}!`, newDevice)
            return util.send(res)
        } catch (error) {
            util.setError(500, `Internal error`)
            return util.send(res)
        }
    }

    static async update_device_right(req, res) {
        const { idHouse, rightToWrite } = req.body
        const { idDevice, idUser } = req.params
        const user = req.user

        if (!idHouse) {
            util.setError(400, `Please provide an idHouse`)
            return util.send(res)
        }
        if (typeof rightToWrite !== 'boolean') {
            util.setError(400, `Please provide a rightToWrite`)
            return util.send(res)
        }

        try {
            const newDevice = await DeviceService.update_device_right(
                idHouse,
                parseInt(idDevice),
                user.idUser,
                parseInt(idUser),
                rightToWrite
            )
            if (newDevice === 'house') {
                util.setError(409, `User doesn\'t have rights in the house`)
                return util.send(res)
            }
            if (newDevice === 'houseUser') {
                util.setError(409, `User selected doesn\'t exist in the house`)
                return util.send(res)
            }
            if (newDevice === 'device') {
                util.setError(403, `Device doesn\'t exist`)
                return util.send(res)
            }
            if (newDevice === 'deviceRight') {
                util.setError(409, `User doesn\'t have rights to modify device`)
                return util.send(res)
            }
            util.setSuccess(200, `Device ${idDevice} changed right to ${rightToWrite} for user ${idUser}!`, newDevice)
            return util.send(res)
        } catch (error) {
            util.setError(500, `Internal error`)
            return util.send(res)
        }
    }

    static async delete_device(req, res) {
        const { id } = req.params
        const user = req.user

        try {
            const deviceToDelete = await DeviceService.delete_device(id, user.idUser)
            if (deviceToDelete === 'noDevice') {
                util.setError(404, `User doesn\'t have rights in the house`)
                return util.send(res)
            }
            if (deviceToDelete === 'house') {
                util.setError(409, `User doesn\'t have rights in the house`)
                return util.send(res)
            }
            if (deviceToDelete === 'device') {
                util.setError(409, `User doesn\'t have rights to modify the device`)
                return util.send(res)
            }
            util.setSuccess(200, `Device deleted ${id}`)
            return util.send(res)
        } catch (error) {
            util.setError(500, `Internal error`)
            return util.send(res)
        }
    }
}

module.exports = DeviceController
