import * as React from 'react'
import { FunctionComponent, useState } from 'react'
import { SafeAreaView, StyleSheet, View } from 'react-native'
import { Button, Input, Text } from 'react-native-elements'

interface Props {
    onPress: (type: string, body: any) => void
    type: 'login' | 'register'
    switchLogin: (bool: boolean) => void
}

export const LoginComponent: FunctionComponent<Props> = ({ onPress, type, switchLogin }) => {
    React.useEffect(() => {}, [])

    const [login, setLogin] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [confirmPassword, setConfirmPassword] = useState<string>('')

    return (
        <SafeAreaView>
            <View style={styles.component}>
                <Input
                    placeholder="Email / Username"
                    leftIcon={{ type: 'font-awesome', name: 'user' }}
                    onChangeText={value => setLogin(value)}
                    errorStyle={{ color: 'red' }}
                    style={styles.input}
                    //errorMessage="Username incorrecte"
                />
                <Input
                    placeholder="Password"
                    leftIcon={{ type: 'font-awesome', name: 'lock' }}
                    onChangeText={value => setPassword(value)}
                    secureTextEntry={true}
                    errorStyle={{ color: 'red' }}
                    style={styles.input}
                    //errorMessage="Password inccorecte"
                />
                <Button
                    title="Login"
                    onPress={() => onPress(type, { login, password, confirmPassword })}
                    disabled={login.length < 1 || password.length < 1}
                    style={{ alignSelf: 'center', width: '85%' }}
                />
                <Text h4 onPress={() => switchLogin(false)}>
                    {"Don't have an account ? Click here"}
                </Text>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    component: {
        width: '100%',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    input: {
        color: '#fff',
        backgroundColor: '#000',
        borderRadius: 4,
    },
    image: {
        flex: 1,
        display: 'flex',
        height: '100%',
        width: '100%',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
})
