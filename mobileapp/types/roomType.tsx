import { RoomEnum } from '../types/enums/RoomEnums'

export interface RoomType {
    idRoom: number
    roomName: string
    roomType: RoomEnum
}

export const defaultRooms: RoomType = {
    idRoom: 0,
    roomName: '',
    roomType: 0,
}
