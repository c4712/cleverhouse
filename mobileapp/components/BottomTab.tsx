import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import * as React from 'react'
import { useState } from 'react'
import HomeScreen from '../screens/HomeScreen'
import MoreScreen from '../screens/MoreScreen'
import PlanningScreen from '../screens/PlanningScreen'
import { TabBarIcon } from './TabBarIcon'

const Tab = createBottomTabNavigator()

export default function BottomTab() {
    const [showMore, setShowMore] = useState<boolean>(false)
    return (
        <Tab.Navigator
            screenOptions={{
                tabBarStyle: {
                    flexDirection: 'row',
                    justifyContent: 'center',
                    height: 50,
                    bottom: 20,
                    left: 20,
                    right: 20,
                    position: 'absolute',
                    backgroundColor: '#B4A7AF',
                    borderRadius: 10,
                    elevation: 5,
                    borderTopWidth: 0,
                    shadowColor: '#575757',
                    shadowOffset: { width: 0, height: 5 },
                    shadowOpacity: 0.75,
                    shadowRadius: 5,
                },
                tabBarActiveTintColor: '#e91e63',
                tabBarInactiveTintColor: '#000',
            }}
        >
            <Tab.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    title: 'Home',
                    tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />,
                    headerShown: false,
                }}
            />
            <Tab.Screen
                name="Planning"
                component={PlanningScreen}
                options={{
                    title: 'Planning',
                    tabBarIcon: ({ color }) => <TabBarIcon name="table" color={color} />,
                    headerShown: false,
                }}
                listeners={({ navigation }) => ({
                    tabPress: e => {
                        setShowMore(false)
                    },
                })}
            />
            <Tab.Screen
                name="More"
                component={MoreScreen}
                options={{
                    title: 'More',
                    tabBarIcon: ({ color }) => <TabBarIcon name="plus" color={color} />,
                    headerShown: false,
                }}
                listeners={({ navigation }) => ({
                    tabPress: e => {
                        setShowMore(false)
                    },
                })}
            />
        </Tab.Navigator>
    )
}
