//During the test the env variable is set to test
require('dotenv').config()
process.env.NODE_ENV = 'test'

let server = require('../app')

//Require the dev-dependencies
let chai = require('chai')
let chaiHttp = require('chai-http')
let should = chai.should()

chai.use(chaiHttp)

const db = require('../database/database')
db.sequelize.options.logging = false

const API_URL = '/houses'

const jwt = require('jsonwebtoken')
const { variables } = require('../config/passport.js')

const idHouse = 0
const nameHouse = 'clvrTestHouse'
let jwtToken = ''
let jwtToken_2 = ''
let idUser = 0
let idUser_2 = 0

//Our parent block
describe('Houses', () => {
    before(async () => {
        await db.House.destroy({ where: {}, truncate: { cascade: true } })
        await db.User.destroy({ where: {}, truncate: { cascade: true } })
        const user = {
            username: 'testCreate',
            email: 'testCreate@clvrhouse.com',
            password: 'testPwd',
        }
        const user_2 = {
            username: 'testCreate2',
            email: 'testCreate2@clvrhouse.com',
            password: 'testPwd',
        }
        await db.User.create({ ...user }).then(async res => {
            idUser = res.dataValues.idUser

            jwtToken = jwt.sign({ sub: idUser, id: idUser }, variables.secretOrKey, {
                expiresIn: '1w',
            })
            await db.User.create({ ...user_2 }).then(async res => {
                idUser_2 = res.dataValues.idUser

                jwtToken_2 = jwt.sign({ sub: idUser_2, id: idUser_2 }, variables.secretOrKey, {
                    expiresIn: '1w',
                })
            })
        })
    })
    beforeEach(async () => {
        await db.House.destroy({ truncate: { cascade: true } })
    })
    after(async () => {
        await db.User.destroy({ truncate: { cascade: true } })
        await db.House.destroy({ truncate: { cascade: true } })
    })

    describe('GET /', () => {
        it('it should NOT GET house if token not provided', done => {
            chai.request(server)
                .get(`${API_URL}/`)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT GET house if token not valid', done => {
            chai.request(server)
                .get(`${API_URL}/`)
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT GET house if not house found', done => {
            chai.request(server)
                .get(`${API_URL}/`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(404)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should GET houses', done => {
            chai.request(server)
                .post(`${API_URL}/`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    chai.request(server)
                        .get(`${API_URL}/`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .end((err, res) => {
                            res.should.have.status(200)
                            res.body.should.have.property('status').eql('success')
                            res.body.should.have.property('data').that.is.an('array')
                            done()
                        })
                })
        })
    })

    describe('GET /:id', () => {
        it('it should NOT GET house if token not provided', done => {
            chai.request(server)
                .get(`${API_URL}/1`)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT GET house if token not valid', done => {
            chai.request(server)
                .get(`${API_URL}/1`)
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT GET house if house not found', done => {
            chai.request(server)
                .get(`${API_URL}/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(404)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should GET house', done => {
            chai.request(server)
                .post(`${API_URL}/`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    chai.request(server)
                        .get(`${API_URL}/${res.body.data.house.idHouse}`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .end((err, res) => {
                            res.should.have.status(200)
                            res.body.should.have.property('status').eql('success')
                            res.body.should.have.property('data').that.is.an('object')
                            done()
                        })
                })
        })
    })

    describe('POST /', () => {
        it('it should NOT POST house if token not provided', done => {
            chai.request(server)
                .post(`${API_URL}/`)
                .send({ nameHouse })
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT POST house if token not valid', done => {
            chai.request(server)
                .post(`${API_URL}/`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT POST house if no nameHouse provided', done => {
            chai.request(server)
                .post(`${API_URL}/`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT POST house if no nameHouse already taken', done => {
            chai.request(server)
                .post(`${API_URL}/`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    chai.request(server)
                        .post(`${API_URL}/`)
                        .send({ nameHouse })
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .end((err, res) => {
                            res.should.have.status(409)
                            res.body.should.have.property('status').eql('error')
                            done()
                        })
                })
        })
        it('it should POST house', done => {
            chai.request(server)
                .post(`${API_URL}/`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(201)
                    res.body.should.have.property('status').eql('success')
                    res.body.should.have.property('data').that.is.an('object')
                    done()
                })
        })
    })

    describe('POST /user/:idUser', () => {
        it('it should NOT POST user in house if token not provided', done => {
            chai.request(server)
                .post(`${API_URL}/house/0`)
                .send({ nameHouse })
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT POST user in house if token not valid', done => {
            chai.request(server)
                .post(`${API_URL}/house/0`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT POST user in house if idHouse not provided', done => {
            chai.request(server)
                .post(`${API_URL}/house`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idUser })
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT POST user in house if idUser not provided', done => {
            chai.request(server)
                .post(`${API_URL}/house/0`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT POST user in house if no house found', done => {
            chai.request(server)
                .post(`${API_URL}/house/0`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idUser })
                .end((err, res) => {
                    res.should.have.status(404)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it("it should NOT POST user in house if bearer doesn't have rights in house", done => {
            chai.request(server)
                .post(`${API_URL}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ nameHouse })
                .end((err, res) => {
                    chai.request(server)
                        .post(`${API_URL}/house/${res.body.data.house.idHouse}`)
                        .set('Authorization', 'Bearer ' + jwtToken_2)
                        .send({ idUser })
                        .end((err, res) => {
                            res.should.have.status(403)
                            res.body.should.have.property('status').eql('error')
                            res.body.should.have.property('message').eql("You don't have the rights to add an user")
                            done()
                        })
                })
        })
        it('it should NOT POST user in house if user already exists in house', done => {
            chai.request(server)
                .post(`${API_URL}`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    chai.request(server)
                        .post(`${API_URL}/house/${res.body.data.house.idHouse}`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .send({ idUser })
                        .end((err, res) => {
                            res.should.have.status(403)
                            res.body.should.have.property('status').eql('error')
                            done()
                        })
                })
        })
        it('it should POST user in house and isMain is true if provided as true', done => {
            chai.request(server)
                .post(`${API_URL}`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    chai.request(server)
                        .post(`${API_URL}/house/${res.body.data.house.idHouse}`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .send({ idUser: idUser_2, isMain: true })
                        .end((err, res) => {
                            res.should.have.status(201)
                            res.body.should.have.property('status').eql('success')
                            res.body.should.have.property('data').that.is.an('object')
                            res.body.data.newUser.should.have.property('isMain').eql(true)
                            done()
                        })
                })
        })
        it('it should POST user in house and isMain is false if not provided ', done => {
            chai.request(server)
                .post(`${API_URL}`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    chai.request(server)
                        .post(`${API_URL}/house/${res.body.data.house.idHouse}`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .send({ idUser: idUser_2 })
                        .end((err, res) => {
                            res.should.have.status(201)
                            res.body.should.have.property('status').eql('success')
                            res.body.should.have.property('data').that.is.an('object')
                            res.body.data.newUser.should.have.property('isMain').eql(false)
                            done()
                        })
                })
        })
    })

    describe('PUT /:id', () => {
        it('it should NOT PUT house if token not provided', done => {
            chai.request(server)
                .put(`${API_URL}/${idHouse}`)
                .send({ nameHouse })
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT PUT house if token not valid', done => {
            chai.request(server)
                .put(`${API_URL}/${idHouse}`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT PUT house if not house found', done => {
            chai.request(server)
                .put(`${API_URL}/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(404)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should PUT house', done => {
            chai.request(server)
                .post(`${API_URL}`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    chai.request(server)
                        .put(`${API_URL}/${res.body.data.house.idHouse}`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .send({ nameHouse: 'TestUpdatePut' })
                        .end((err, res) => {
                            res.should.have.status(200)
                            res.body.should.have.property('status').eql('success')
                            res.body.should.have.property('data').that.is.an('object')
                            res.body.data.should.have.property('nameHouse').eql('TestUpdatePut')
                            done()
                        })
                })
        })
    })

    describe('DELETE /:id', () => {
        it('it should NOT DELETE house if token not provided', done => {
            chai.request(server)
                .delete(`${API_URL}/${idHouse}`)
                .send({ nameHouse })
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT DELETE house if token not valid', done => {
            chai.request(server)
                .delete(`${API_URL}/${idHouse}`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT DELETE house if idHouse not provided', done => {
            chai.request(server)
                .delete(`${API_URL}/`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT DELETE house if not house found', done => {
            chai.request(server)
                .delete(`${API_URL}/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(404)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it("it should NOT DELETE house if user doesn't have rights", done => {
            chai.request(server)
                .post(`${API_URL}/`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    let this_idHouse = res.body.data.house.idHouse
                    console.log
                    chai.request(server)
                        .post(`${API_URL}/house/${this_idHouse}`)
                        .send({ idUser: idUser_2, isMain: false })
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .end((err, res) => {
                            chai.request(server)
                                .delete(`${API_URL}/${this_idHouse}`)
                                .set('Authorization', 'Bearer ' + jwtToken_2)
                                .end((err, res) => {
                                    res.should.have.status(403)
                                    res.body.should.have.property('status').eql('error')
                                    done()
                                })
                        })
                })
        })
        it('it should DELETE house', done => {
            chai.request(server)
                .post(`${API_URL}/`)
                .send({ nameHouse })
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    chai.request(server)
                        .delete(`${API_URL}/${res.body.data.house.idHouse}`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .end((err, res) => {
                            res.should.have.status(200)
                            res.body.should.have.property('status').eql('success')
                            done()
                        })
                })
        })
    })
})
