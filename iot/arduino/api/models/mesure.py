from typing import List, Union

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Mesure(BaseModel):
    unite: Union[str,None]
    idDevice: Union[int,None]
    typeMesure: Union[str,None]
    valueMesure: Union[bool,int,str,None]
