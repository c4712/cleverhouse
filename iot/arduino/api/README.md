# Api IOT

## <ins>How to run api</ins>
* Step 1: In api repository use this command : ". env/bin/activate" 
* Step 2: You can see "(env)" 
* Step 3: Use command "uvicorn api:app --reload"
* Step 4: You have access to use the api

## <ins>Access to doc: </ins>
* http://127.0.0.1:8000/docs

# Repository api:
* models: Class who refers Object of Database table 
* api: Contain all the routes (GET, POST, PUT)

