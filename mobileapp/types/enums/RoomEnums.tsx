/* import bathtub from '../../assets/images/roomIcons/bathtub.png'
import bed from '../../assets/images/roomIcons/bed.png'
import hallway from '../../assets/images/roomIcons/hallway.png'
import kitchen from '../../assets/images/roomIcons/kitchen.png'
 */
export enum RoomEnum {
    BATHROOM = 1,
    ROOM = 2,
    KITCHEN = 3,
    HALLWAY = 4,
}

export const RoomLogo = {
    [RoomEnum.BATHROOM]:
        'https://mpng.subpng.com/20200216/yfx/transparent-incandescent-light-bulb-led-lamp-logo-royalty-free-5ea8f17468f081.3469125215881301644298.jpg',
    [RoomEnum.ROOM]:
        'https://mpng.subpng.com/20200216/yfx/transparent-incandescent-light-bulb-led-lamp-logo-royalty-free-5ea8f17468f081.3469125215881301644298.jpg',
    [RoomEnum.KITCHEN]:
        'https://mpng.subpng.com/20200216/yfx/transparent-incandescent-light-bulb-led-lamp-logo-royalty-free-5ea8f17468f081.3469125215881301644298.jpg',
    [RoomEnum.HALLWAY]:
        'https://mpng.subpng.com/20200216/yfx/transparent-incandescent-light-bulb-led-lamp-logo-royalty-free-5ea8f17468f081.3469125215881301644298.jpg',
}
