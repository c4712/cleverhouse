export interface Mesure {
    idDevice: number
    typeMesure: string
    unite: string
    valueMesure: boolean
}
