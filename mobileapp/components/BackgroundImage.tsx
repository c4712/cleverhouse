import React from 'react'
import { Dimensions, Image } from 'react-native'

const BackgroundImage = (props: any) => {
    const window = Dimensions.get('window')
    const height = props.height || window.height
    const width = window.width
    const resizeMode = props.resizeMode || 'cover' // cover
    return (
        <Image
            style={[props.styles, { height: height - props.headerSize, width: null, resizeMode: resizeMode }]}
            source={props.uri ? { uri: props.uri } : props.source}
        >
            {props.children}
        </Image>
    )
}

export default BackgroundImage
