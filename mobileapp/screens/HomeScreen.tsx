import * as React from 'react'
import { useEffect, useState } from 'react'
import { ImageBackground, StyleSheet, Switch, TouchableOpacity, View } from 'react-native'
import { Divider, Text } from 'react-native-elements'
import { getDevicesFromAPI } from '../api/devices'
import { getDatasFromIOT } from '../api/iot'
import { DefaultModal } from '../components/DefaultModal'
import DeviceComponent from '../components/Device'
import Rooms from '../components/Room'
import { DeviceType } from '../types/devices'
import { DeviceLogo } from '../types/enums/DeviceEnums'
import { RoomLogo } from '../types/enums/RoomEnums'
import { RoomType } from '../types/roomType'
import { getItem } from '../utils/asyncStorage'

export default function HomeScreen({ ...props }: any) {
    const [token, setToken] = useState()

    const setItem = async () => {
        const token = await getItem('jwtToken')
        setToken(token)
        if (!token) props.navigation.navigate('Login')
    }

    useEffect(() => {
        setItem()
    }, [])

    const [devices, setDevices] = useState<DeviceType[]>([])
    const [rooms, setRooms] = useState<RoomType[]>([])

    const [selectedRoom, setSelectedRoom] = useState<RoomType | null>(null)
    const [selectedDevice, setSelectedDevice] = useState<DeviceType | null>(null)
    const [isEnabled, setIsEnabled] = useState<boolean>(false)

    const getAllDatas = async () => {
        const devices = await getDevicesFromAPI()
        const res = await getDatasFromIOT()
        setDevices(res.devices)
        setRooms(res.rooms)
    }

    useEffect(() => {
        getAllDatas()
    }, [])

    useEffect(() => {
        if (rooms.length) setSelectedRoom(rooms[0])
    }, [rooms])

    const [isModalVisible, setIsModalVisible] = React.useState(false)
    const handleModal = (device?: DeviceType) => {
        if (device?.idDevice) {
            setIsModalVisible(old => !old)
            setSelectedDevice(device)
        }
    }

    return (
        <View style={styles.container}>
            <ImageBackground
                source={{ uri: 'https://alienor-avocats.com/wp-content/uploads/2020/05/p3-gallery1.jpg' }}
                resizeMode="cover"
                style={styles.image}
            >
                <View style={{ display: 'flex', width: '100%' }}>
                    <Text h2>Rooms</Text>
                    <View style={{ flexDirection: 'row' }}>
                        {rooms.map((item, index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => setSelectedRoom(item)}>
                                    <Rooms
                                        label={item.roomName}
                                        icon={RoomLogo[item.roomType]}
                                        selected={selectedRoom ? selectedRoom?.idRoom === item.idRoom : false}
                                    />
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                </View>
                <Divider width={3} color={'black'} />
                <View>
                    <Text h2>Devices</Text>
                    {devices
                        .filter(item => item.idRoom === selectedRoom?.idRoom)
                        .map((item, index) => (
                            <TouchableOpacity key={index} onPress={() => handleModal(item)}>
                                <DeviceComponent
                                    label={item.deviceName}
                                    selected={selectedDevice ? selectedDevice?.idDevice === item.idDevice : false}
                                    icon={DeviceLogo[item.deviceType]}
                                />
                            </TouchableOpacity>
                        ))}
                </View>
                <DefaultModal
                    isVisible={isModalVisible}
                    onBackdropPress={() => {
                        setSelectedDevice(null)
                        setIsModalVisible(false)
                    }}
                >
                    <DefaultModal.Container>
                        <DefaultModal.Header title={selectedDevice?.deviceName ?? ''} />
                        <DefaultModal.Body>
                            <View style={styles.modalBody}>
                                <Switch
                                    trackColor={{ false: '#767577', true: '#81b0ff' }}
                                    thumbColor={isEnabled ? '#f5dd4b' : '#f4f3f4'}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={() => setIsEnabled(old => !old)}
                                    value={isEnabled}
                                />
                                <Text style={styles.modalText}>{isEnabled ? 'Active' : 'Inactive'}</Text>
                            </View>
                        </DefaultModal.Body>
                    </DefaultModal.Container>
                </DefaultModal>
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    image: {
        flex: 1,
        display: 'flex',
        height: '100%',
        width: '100%',
    },
    text: {
        fontSize: 16,
        fontWeight: '400',
        textAlign: 'center',
    },
    modalBody: {
        display: 'flex',
        flexDirection: 'row',
    },
    modalText: {
        fontSize: 16,
        fontWeight: '400',
        marginLeft: '1rem',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
})
