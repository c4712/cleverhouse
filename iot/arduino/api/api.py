import json
from enum import Enum
from typing import Any, Dict, List, Optional, Union

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from models.device import Device
from models.mesure import Mesure
from models.room import Room

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins='*',
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class RoomType(Enum):
    BATHROOM = 1
    ROOM = 2
    KITCHEN = 3
    HALLWAY = 4

class DeviceType(Enum):
    LAMP = 1
    HEATER = 2
    THERMOMETER = 3
    SHUTTER = 4

devices = [
    {"idDevice": 1, "deviceName": "Lampe", "idRoom": 1,"deviceType": DeviceType.LAMP.value },
    {"idDevice": 2, "deviceName": "Volet", "idRoom": 1, "deviceType": DeviceType.SHUTTER.value },
    {"idDevice": 3, "deviceName": "Lampe2", "idRoom": 1, "deviceType": DeviceType.LAMP.value },
    {"idDevice": 4, "deviceName": "Chauffage", "idRoom": 1, "deviceType": DeviceType.HEATER.value },
    {"idDevice": 5, "deviceName": "Thermometre", "idRoom": 1, "deviceType": DeviceType.THERMOMETER.value },

    {"idDevice": 6, "deviceName": "Volet1", "idRoom": 2, "deviceType": DeviceType.SHUTTER.value },
    {"idDevice": 7, "deviceName": "Lampe1", "idRoom": 2, "deviceType": DeviceType.LAMP.value },
    {"idDevice": 8, "deviceName": "Volet2", "idRoom": 2, "deviceType": DeviceType.SHUTTER.value },
    {"idDevice": 9, "deviceName": "Lampe2", "idRoom": 2, "deviceType": DeviceType.LAMP.value },
    {"idDevice": 10, "deviceName": "Lampe3", "idRoom": 2, "deviceType": DeviceType.LAMP.value },

    {"idDevice": 11, "deviceType": DeviceType.LAMP.value },
    {"idDevice": 12, "deviceType": DeviceType.LAMP.value },
]

rooms = [
    {"idRoom": 1, "roomName": "Chambre", "roomType": RoomType.ROOM.value },
    {"idRoom": 2, "roomName": "Cuisine", "roomType": RoomType.KITCHEN.value },
]

mesures = [
    {"unite": "switch", "idDevice": 1, "typeMesure": "boolean", "valueMesure": False},
    {"unite": "switch", "idDevice": 2, "typeMesure": "boolean", "valueMesure": False},
    {"unite": "switch", "idDevice": 3, "typeMesure": "boolean", "valueMesure": False},
    {"unite": "switch", "idDevice": 4, "typeMesure": "boolean", "valueMesure": False},
    {"unite": "switch", "idDevice": 5, "typeMesure": "boolean", "valueMesure": False},

    {"unite": "switch", "idDevice": 6, "typeMesure": "boolean", "valueMesure": False},
    {"unite": "switch", "idDevice": 7, "typeMesure": "boolean", "valueMesure": False},
    {"unite": "switch", "idDevice": 8, "typeMesure": "boolean", "valueMesure": False},
    {"unite": "switch", "idDevice": 9, "typeMesure": "boolean", "valueMesure": False},
    {"unite": "switch", "idDevice": 10, "typeMesure": "boolean", "valueMesure": False},

    {"unite": "switch", "idDevice": 11, "typeMesure": "boolean", "valueMesure": False},
    {"unite": "switch", "idDevice": 12, "typeMesure": "boolean", "valueMesure": False},
]

## ROUTE TEST

@app.get("/api/")
def read_root():
    return {"Code": "200"}

## GET COLLECTION ROUTES 

@app.get("/api/all/")
async def read_devices():
    return {"devices": devices, "rooms": rooms}


@app.get("/api/devices/", response_model=List)
async def read_devices():
    ##devices = await db["devices"].find().to_list(1000)
    devicesMesures = []
    for i in range(len(devices)):
        devicesMesures.append({"device": {**devices[i]}, "mesure": {**mesures[i]}})
    return devicesMesures

@app.get("/api/rooms/", response_model=List[Room])
async def read_rooms():
    return rooms

@app.get("/api/mesures/", response_model=List[Mesure])
async def read_mesures():
    return mesures


## GET ITEM ROUTES 

@app.get("/api/device/{device_id}")
def read_device_item(device_id: int, q: Optional[str] = None):
    return {"device_id": device_id, "q": q}

@app.get("/api/room/{room_id}")
def read_room_item(room_id: int, q: Optional[str] = None):
    return {"room_id": room_id, "q": q}

@app.get("/api/mesure/{mesure_id}")
def read_mesure_item(mesure_id: int, q: Optional[str] = None):
    return {"mesure_id": mesure_id, "q": q}

@app.get("/api/mesure/{mesure_id}/device/{device_id}")
def read_mesure_of_device(mesure_id: int, q: Optional[str] = None):
    return {"mesure_id": mesure_id, "q": q}

@app.get("/api/device/{device_id}/room/{room_id}")
def read_device_of_room(room_id: int, q: Optional[str] = None):
    return {"room_id": room_id, "q": q}

## POST ROUTES 

@app.post("/device")
async def create_device(device: Device):
    return device

@app.post("/room")
async def create_room(room: Room):
    return room

@app.post("/mesure")
async def create_mesure(mesure: Mesure):
    return mesure

## PUT ITEM ROUTES 

@app.put("/device/{idDevice}")
def update_device(idDevice: int, device: Device):
    return {"deviceName": device.deviceName, "idDevice": idDevice}

@app.put("/room/{idRoom}")
def update_room(idRoom: int, room: Room):
    return {"roomName": room.roomName, "idRoom": idRoom}

@app.put("/mesure/{idMesure}")
def update_mesure(idMesure: int, mesure: Mesure):
    return {"idDevice": idMesure, "typeMesure": mesure.typeMesure, "unite": mesure.unite, "valueMesure": mesure.valueMesure}


## PATCH ITEM ROUTES

@app.patch("/api/device/{idDevice}", response_model=Mesure)
def patch_device(idDevice: int, mesure: Mesure):
    print(idDevice)
    mesure_index = next((i for i, item in enumerate(mesures) if item["idDevice"] == idDevice), None)
    mesure_update = {}
    for key, value in mesure:
        if value != None:
            mesure_update[key] = value
    mesures[mesure_index] = {**mesures[mesure_index], **mesure_update}
    return {**mesures[mesure_index], **mesure_update}
