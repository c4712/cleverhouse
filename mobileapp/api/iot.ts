import { GET_REQUEST, IOT_URL } from '../config/api'
import getAxios from './axios'

const IOT_ALL_URL = `${IOT_URL}`

const axios = getAxios()

const getDatasFromIOT = async () => {
    const resp = await axios.get(`${IOT_ALL_URL}/all`, GET_REQUEST)
    return resp.data
}

export { getDatasFromIOT }
