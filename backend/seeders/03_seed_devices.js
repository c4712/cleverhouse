'use strict'

module.exports = {
    up: async (queryInterface, Sequelize) => {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        return await queryInterface.bulkInsert(
            'Devices',
            [
                {
                    idDevice: 1,
                    idHouse: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idDevice: 3,
                    idHouse: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idDevice: 124,
                    idHouse: 2,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idDevice: 782,
                    idHouse: 3,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {}
        )
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        return await queryInterface.bulkDelete('Devices', null, {})
    },
}
