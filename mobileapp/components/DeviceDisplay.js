import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

const DeviceDisplay = props => {
    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                <Image
                    source={{ uri: props.icon }}
                    resizeMode="contain"
                    style={{ width: '12vw', height: '12vw', marginHorizontal: '1rem' }}
                />
                <Text style={styles.itemText}>{props.text}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        padding: '0.5rem',
        borderRadius: 20,
        marginBottom: 10,
        width: '80vw',
        opacity: 0.85,
    },
    itemLeft: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    itemText: {
        maxWidth: '80%',
        fontSize: 15,
    },
})

export default DeviceDisplay
