/* import heater from 'mobileapp/assets/images/deviceIcons/heater.png'
import lamp from '../../assets/images/deviceIcons/lamp.png'
import thermometer from '../../assets/images/deviceIcons/thermometer.png'
import shutter from '../../assets/images/deviceIcons/window.png'
 */
export enum DeviceEnum {
    LAMP = 1,
    HEATER = 2,
    THERMOMETER = 3,
    SHUTTER = 4,
}

export const DeviceLogo = {
    [DeviceEnum.LAMP]:
        'https://mpng.subpng.com/20200216/yfx/transparent-incandescent-light-bulb-led-lamp-logo-royalty-free-5ea8f17468f081.3469125215881301644298.jpg',
    [DeviceEnum.HEATER]:
        'https://mpng.subpng.com/20200216/yfx/transparent-incandescent-light-bulb-led-lamp-logo-royalty-free-5ea8f17468f081.3469125215881301644298.jpg',
    [DeviceEnum.THERMOMETER]:
        'https://mpng.subpng.com/20200216/yfx/transparent-incandescent-light-bulb-led-lamp-logo-royalty-free-5ea8f17468f081.3469125215881301644298.jpg',
    [DeviceEnum.SHUTTER]:
        'https://mpng.subpng.com/20200216/yfx/transparent-incandescent-light-bulb-led-lamp-logo-royalty-free-5ea8f17468f081.3469125215881301644298.jpg',
}
