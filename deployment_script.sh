
#! /bin/sh
value=$(docker ps | grep dat_db )
if [ "x${value}x" != "xx" ]
then
    echo "dat_db : pause + suppression "
    docker stop dat_db 
    docker rm dat_db
fi
value=$(docker ps | grep dat_back )
if [ "x${value}x" != "xx" ]
then
    echo "dat_back : pause + suppression "
    docker stop dat_back 
    docker rm dat_back
fi
value=$(docker ps -l | grep dat_db )
if [ "x${value}x" != "xx" ]
then
    echo "dat_db : suppression "
    docker rm dat_db
fi
value=$(docker ps -l | grep dat_back )
if [ "x${value}x" != "xx" ]
then
    echo "dat_back : suppression "
    docker rm dat_back
fi
value=$(docker ps | grep dat_front )
if [ "x${value}x" != "xx" ]
then
    echo "dat_front : pause + suppression "
    docker stop dat_front
    docker rm dat_front
fi
value=$(docker ps | grep dat_front )
value=$(docker ps -l | grep dat_front )
if [ "x${value}x" != "xx" ]
then
    echo "dat_front : suppression "
    docker rm dat_front
fi
docker system prune --force
docker network create my-network

