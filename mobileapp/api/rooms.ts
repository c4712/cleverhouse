import { AxiosResponse } from 'axios'
import { GET_REQUEST, IOT_URL } from '../config/api'
import { Rooms } from '../types/rooms'
import getAxios from './axios'

const ROOMS_URL = `${IOT_URL}/api/rooms`

const axios = getAxios()

const getRooms = async (): Promise<AxiosResponse<Rooms[]>> => {
    return await axios.get(`${ROOMS_URL}`, GET_REQUEST)
}

export { getRooms }
