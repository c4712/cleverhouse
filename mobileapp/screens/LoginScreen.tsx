import * as React from 'react'
import { FunctionComponent, useState } from 'react'
import { ImageBackground, StyleSheet, View } from 'react-native'
import { login, register } from '../api/auth'
import { LoginComponent } from '../components/LoginComponent'
import { RegisterComponent } from '../components/RegisterComponent'
import { setItem } from '../utils/asyncStorage'

export const LoginScreen: FunctionComponent = ({ ...props }: any) => {
    const [isLogin, setIsLogin] = useState<boolean>(true)

    const onConnect = async (type: string, body: any) => {
        switch (type) {
            case 'login':
                await login(body).then(loginRes => {
                    setItem('jwtToken', loginRes.data.jwtToken)
                    props.navigation.navigate('Root')
                })
                break
            case 'register':
                await register(body).then(async res => {
                    await login({ login: res.data.email, password: body.password }).then(loginRes => {})
                })
                break
            default:
                return
        }
    }

    return (
        <View style={styles.container}>
            {/*  <ScreenContainer style={styles.container}> */}
            <ImageBackground
                source={{ uri: 'https://alienor-avocats.com/wp-content/uploads/2020/05/p3-gallery1.jpg' }}
                resizeMode="cover"
                style={styles.image}
            >
                <View style={styles.component}>
                    {isLogin ? (
                        <LoginComponent type={'login'} onPress={onConnect} switchLogin={setIsLogin} />
                    ) : (
                        <RegisterComponent type={'register'} onPress={onConnect} switchLogin={setIsLogin} />
                    )}
                </View>
            </ImageBackground>
            {/* </ScreenContainer> */}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        flex: 1,
        display: 'flex',
        height: '100%',
        width: '100%',
        justifyContent: 'center',
    },
    component: {
        display: 'flex',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
})
