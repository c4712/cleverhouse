import { AxiosResponse } from 'axios'
import { GET_REQUEST, IOT_URL, PATCH_REQUEST, SERVER_URL } from '../config/api'
import { DeviceType } from '../types/devices'
import { Mesure } from '../types/mesures'
import getAxios from './axios'

const DEVICES_URL = `${SERVER_URL}/devices`
const DEVICES_IOT_URL = `${IOT_URL}/device`
const MESURES_IOT_URL = `${IOT_URL}/mesures`

const axios = getAxios()

const getDevices = async (): Promise<AxiosResponse<DeviceType[]>> => {
    const resp = await axios.get(`${DEVICES_URL}`, GET_REQUEST)
    return resp.data
}

const getDevicesFromAPI = async (): Promise<AxiosResponse<{ device: DeviceType; mesure: Mesure }[]>> => {
    return await axios.get(`${DEVICES_IOT_URL}s`, GET_REQUEST)
}

const patchDeviceFromIot = async (id: number, value: Partial<Mesure>): Promise<AxiosResponse<DeviceType[]>> => {
    return await axios.patch(`${DEVICES_IOT_URL}/${id}`, { ...value }, PATCH_REQUEST)
}

export { getDevices, getDevicesFromAPI, patchDeviceFromIot }
