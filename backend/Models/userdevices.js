const sequelize = require('sequelize')
const Sequelize = require('sequelize')

module.exports = (sequelize, DataTypes) => {
    const UserDevices = sequelize.define(
        'UserDevices',
        {
            /**
             * Helper method for defining associations.
             * This method is not a part of Sequelize lifecycle.
             * The `models/index` file will call this method automatically.
             */
            idUserDevices: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            idUser: {
                allowNull: false,
                foreignKey: true,
                type: Sequelize.INTEGER,
            },
            idDevice: {
                allowNull: false,
                foreignKey: true,
                type: Sequelize.INTEGER,
            },
            rightToWrite: {
                allowNull: false,
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
        },
        { freezeTableName: true }
    )
    UserDevices.associate = function (models) {
        UserDevices.belongsTo(models.User, { foreignKey: 'idUser', as: 'user', onDelete: 'cascade', hooks: true })
        UserDevices.belongsTo(models.Device, { foreignKey: 'idDevice', as: 'device' })
    }
    return UserDevices
}
