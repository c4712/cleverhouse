'use strict'

module.exports = {
    up: async (queryInterface, Sequelize) => {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        return await queryInterface.bulkInsert(
            'UserDevices',
            [
                {
                    idUser: 1,
                    idDevice: 1,
                    rightToWrite: true,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idUser: 1,
                    idDevice: 3,
                    rightToWrite: true,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idUser: 1,
                    idDevice: 124,
                    rightToWrite: false,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idUser: 1,
                    idDevice: 782,
                    rightToWrite: true,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idUser: 2,
                    idDevice: 1,
                    rightToWrite: false,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idUser: 3,
                    idDevice: 1,
                    rightToWrite: false,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {}
        )
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        return await queryInterface.bulkDelete('UserDevices', null, {})
    },
}
