const db = require('../database/database')
const { Op } = require('sequelize')

class HouseService {
    static async get_houses_of_user(idUser) {
        try {
            return await db.House.findAll({
                raw: true,
                nest: true,
                include: [
                    {
                        model: db.UserIsMain,
                        as: 'userIsMain',
                        where: {
                            idUser,
                        },
                        attributes: [],
                    },
                ],
            })
        } catch (error) {
            throw error
        }
    }

    static async get_house(id, idUser) {
        try {
            const house = await db.House.findOne({
                where: { idHouse: id },
                include: [
                    {
                        model: db.UserIsMain,
                        as: 'userIsMain',
                        where: {
                            idUser,
                        },
                        attributes: [],
                    },
                ],
                raw: true,
                nest: true,
            })
            return house
        } catch (error) {
            throw error
        }
    }

    static async add_house(idUser, newHouse) {
        try {
            const houseExists = await db.House.findOne({ where: { nameHouse: newHouse.nameHouse } })
            if (houseExists) return 'name'
            const created = await db.House.create(newHouse)
            const createdIsMain = await db.UserIsMain.create({ idUser, idHouse: created.idHouse, isMain: true })
            return { house: { ...created.dataValues, isMain: createdIsMain.isMain } }
        } catch (error) {
            throw error
        }
    }

    static async add_user_to_house(idHouse, idUser, newUserId, isMain) {
        try {
            const houseExists = await db.UserIsMain.findOne({ where: { idHouse: idHouse } })
            if (!houseExists) return 'house'
            const rightsInHouse = await db.UserIsMain.findOne({
                where: { idHouse: idHouse, idUser: idUser, isMain: true },
            })
            if (!rightsInHouse) return 'rights'
            const userExists = await db.UserIsMain.findOne({
                where: { [Op.and]: [{ idHouse: idHouse }, { idUser: newUserId }] },
            })
            if (userExists) return 'exists'
            const created = await db.UserIsMain.create({
                idUser: newUserId,
                idHouse: idHouse,
                isMain: isMain,
            })
            return { newUser: { ...created.dataValues } }
        } catch (error) {
            throw error
        }
    }

    static async update_house(id, updateHouse, idUser) {
        try {
            const houseToUpdate = await db.House.findOne({
                where: { idHouse: id },
                include: [
                    {
                        model: db.UserIsMain,
                        as: 'userIsMain',
                        where: {
                            idUser: idUser,
                            isMain: true,
                        },
                    },
                ],
            })

            if (houseToUpdate) {
                const updatedHouse = await db.House.update(updateHouse, {
                    where: { idHouse: id },
                    returning: true,
                })
                return updatedHouse[1][0]
            }
            return null
        } catch (error) {
            throw error
        }
    }

    static async delete_house(id, idUser) {
        try {
            const houseToDelete = await db.House.findOne({ where: { idHouse: id } })
            if (!houseToDelete) return 'house'

            const userRights = await db.UserIsMain.findOne({ where: { idUser: idUser, idHouse: id, isMain: true } })
            if (!userRights) return 'rights'

            if (houseToDelete) {
                const deletedHouse = await db.House.destroy({
                    where: { idHouse: id },
                })
                return deletedHouse
            }
            return null
        } catch (error) {
            throw error
        }
    }
}

module.exports = HouseService
