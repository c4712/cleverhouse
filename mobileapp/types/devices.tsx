import { DeviceEnum } from '../types/enums/DeviceEnums'

export interface DeviceType {
    deviceName: string
    idDevice: number
    idRoom?: number
    deviceType: DeviceEnum
}

export const defaultRooms: DeviceType = {
    deviceName: '',
    idDevice: 0,
    deviceType: 0,
}
