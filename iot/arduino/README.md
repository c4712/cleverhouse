# Arduino Mega 2560

## <ins>Sensors used: </ins>
* DHT11
## <ins>Other elements used: </ins>
* Led RGB
* Step motor

# Repository iot:
* api: Api that links sensor data to the public api
* librairies: Libraries used in the source code 
* schema: Sensor connection diagram
* SmartBox: Source code that manages the sensors of the arduino board and allows to get their data
* ledRgbProgram: Source code of led RGB and Step motor implementation
