
#include <Stepper.h>

double stepsPerRevolution = 2048;

Stepper myStepper(stepsPerRevolution, 42, 43, 40, 41);

// <------------------ PINS ------------------>

// <---------- Office ---------->

int redPinOffice = 2;
int greenPinOffice = 3;
int bluePinOffice = 4;
int buttonPinOffice = 5;

// <---------- Salon ---------->

int redPinSalon = 6;
int greenPinSalon = 7;
int bluePinSalon = 8;
int buttonPinSalon = 9;

// <---------- Kitchen ---------->

int redPinKitchen = 10;
int greenPinKitchen = 11;
int bluePinKitchen = 12;
int buttonPinKitchen = 13;

// <---------- Bedroom ---------->

int redPinBedroom = 22;
int greenPinBedroom = 23;
int bluePinBedroom = 24;
int buttonPinBedroom = 25;

// <---------- Bathroom ---------->

int redPinBathroom = 26;
int greenPinBathroom = 27;
int bluePinBathroom = 28;
int buttonPinBathroom = 29;

// <------------------ VARIABLES ------------------>

int compteur = 0;
String jsonString = "";
// <---------- Bureau ---------->

static bool buttonOfficeState = false;    
static bool lastButtonOfficeState = false;
static bool ledOfficeState = false;

// <---------- Salon ---------->

static bool buttonSalonState = false;    
static bool lastButtonSalonState = false;
static bool ledSalonState = false;

// <---------- Kitchen ---------->

static bool buttonKitchenState = false;    
static bool lastButtonKitchenState = false;
static bool ledKitchenState = false;

// <---------- Bedroom ---------->

static bool buttonBedroomState = false;    
static bool lastButtonBedroomState = false;
static bool ledBedroomState = false;

// <---------- Bathroom ---------->

static bool buttonBathroomState = false;    
static bool lastButtonBathroomState = false;
static bool ledBathroomState = false;

void setup() {

  Serial.begin(9600);
  myStepper.setSpeed(700);
  //<------- INIT PIN OFFICE ------->

  pinMode(redPinOffice, OUTPUT);
  pinMode(greenPinOffice, OUTPUT);
  pinMode(bluePinOffice, OUTPUT);
  pinMode(buttonPinOffice, OUTPUT);

  //<------- INIT PIN SALON ------->

  pinMode(redPinSalon, OUTPUT);
  pinMode(greenPinSalon, OUTPUT);
  pinMode(bluePinSalon, OUTPUT);
  pinMode(buttonPinSalon, OUTPUT);
  
  //<------- INIT PIN KITCHEN ------->

  pinMode(redPinKitchen, OUTPUT);
  pinMode(greenPinKitchen, OUTPUT);
  pinMode(bluePinKitchen, OUTPUT);
  pinMode(buttonPinKitchen, OUTPUT);

  //<------- INIT PIN BEDROOM ------->

  pinMode(redPinBedroom, OUTPUT);
  pinMode(greenPinBedroom, OUTPUT);
  pinMode(bluePinBedroom, OUTPUT);
  pinMode(buttonPinBedroom, OUTPUT);

  //<------- INIT PIN BATHROOM ------->

  pinMode(redPinBathroom, OUTPUT);
  pinMode(greenPinBathroom, OUTPUT);
  pinMode(bluePinBathroom, OUTPUT);
  pinMode(buttonPinBathroom, OUTPUT);

}

void loop() {
  
  //myStepper.step(stepsPerRevolution);  
  //delay(1000);
   
  
  // <------------- LED OFFICE ------------->
  
  buttonOfficeState = digitalRead(buttonPinOffice);

  if (buttonOfficeState != lastButtonOfficeState) {
    if (buttonOfficeState) { 
      if (compteur == 10) {
        generateEndJson();
        compteur = 0;
        Serial.println(jsonString);
        jsonString = "";
      }
      if  (compteur == 0) {
        generateStartJson();
      }
      if (ledOfficeState) {
        ledOfficeState = false;
        generateJson();
      } else {
        ledOfficeState = true;
        generateJson();
      }
      compteur++;
    }
    lastButtonOfficeState = buttonOfficeState;
  }
 
  if (ledOfficeState) {
    changeColorLed(255, 255, 255, redPinOffice, greenPinOffice, bluePinOffice);
  } else {
    changeColorLed(0, 0, 0, redPinOffice, greenPinOffice, bluePinOffice);
  }

  // <------------- LED SALON ------------->

   buttonSalonState = digitalRead(buttonPinSalon);

  if (buttonSalonState != lastButtonSalonState) {
    if (buttonSalonState) { 
      if (compteur == 10) {
        generateEndJson();
        compteur = 0;
        Serial.println(jsonString);
        jsonString = "";
      }
      if  (compteur == 0) {
        generateStartJson();
      }
      if (ledSalonState) {
        ledSalonState = false;
        generateJson();
      } else {
        ledSalonState = true;
        generateJson();
      }
      compteur++;
    }
    lastButtonSalonState = buttonSalonState;
  }

  if (ledSalonState) {
    changeColorLed(255, 255, 255, redPinSalon, greenPinSalon, bluePinSalon);
  } else {
    changeColorLed(0, 0, 0, redPinSalon, greenPinSalon, bluePinSalon);
  }

  // <------------- LED KITCHEN ------------->

   buttonKitchenState = digitalRead(buttonPinKitchen);

  if (buttonKitchenState != lastButtonKitchenState) {
    if (buttonKitchenState) { 
      if (compteur == 10) {
        generateEndJson();
        compteur = 0;
        Serial.println(jsonString);
        jsonString = "";
      }
      if  (compteur == 0) {
        generateStartJson();
      }
      if (ledKitchenState) {
        ledKitchenState = false;
        generateJson();
      } else {
        ledKitchenState = true;
        generateJson();
      }
      compteur++;
    }
    lastButtonKitchenState = buttonKitchenState;
  }

  if (ledKitchenState) {
    changeColorLed(255, 255, 255, redPinKitchen, greenPinKitchen, bluePinKitchen);
  } else {
    changeColorLed(0, 0, 0, redPinKitchen, greenPinKitchen, bluePinKitchen);
  }

  // <------------- LED BEDROOM ------------->

   buttonBedroomState = digitalRead(buttonPinBedroom);

  if (buttonBedroomState != lastButtonBedroomState) {
    if (buttonBedroomState) { 
      if (compteur == 10) {
        generateEndJson();
        compteur = 0;
        Serial.println(jsonString);
        jsonString = "";
      }
      if  (compteur == 0) {
        generateStartJson();
      }
      if (ledBedroomState) {
        ledBedroomState = false;
        generateJson();
      } else {
        ledBedroomState = true;
        generateJson();
      }
      compteur++;
    }
    lastButtonBedroomState = buttonBedroomState;
  }

  if (ledBedroomState) {
    changeColorLed(255, 255, 255, redPinBedroom, greenPinBedroom, bluePinBedroom);
  } else {
    changeColorLed(0, 0, 0, redPinBedroom, greenPinBedroom, bluePinBedroom);
  }

  // <------------- LED BATHROOM ------------->

   buttonBathroomState = digitalRead(buttonPinBathroom);

  if (buttonBathroomState != lastButtonBathroomState) {
    if (buttonBathroomState) { 
      if (compteur == 10) {
        generateEndJson();
        compteur = 0;
        Serial.println(jsonString);
        jsonString = "";
      }
      if  (compteur == 0) {
        generateStartJson();
      }
      if (ledBathroomState) {
        ledBathroomState = false;
        generateJson();
      } else {
        ledBathroomState = true;
        generateJson();
      }
      compteur++;
    }
    lastButtonBathroomState = buttonBathroomState;
  }

  if (ledBathroomState) {
    changeColorLed(255, 255, 255, redPinBathroom, greenPinBathroom, bluePinBathroom);
  } else {
    changeColorLed(0, 0, 0, redPinBathroom, greenPinBathroom, bluePinBathroom);
  }
}

void changeColorLed(int redValue, int greenValue, int blueValue, int redPin, int greenPin, int bluePin)
 {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}

void generateStartJson() {
  jsonString += "{\n";
}


void generateJson() {
 jsonString += "\t\"system\" : {\n";
 jsonString += "\t\t\"idLightKitchen\" : {\n";
 jsonString += "\t\t\t\"state\" : ";
 jsonString += ledKitchenState;
 jsonString += ",\n";
 jsonString += "\t\t\t\"intensity\": \"100\",\n";
 jsonString += "\t\t\t\"color\" : \"#384646\",\n";
 jsonString += "\t\t},\n";
 jsonString += "\t\t\"idLightSalon\" : {\n";
 jsonString += "\t\t\t\"state\" : ";
 jsonString += ledSalonState;
 jsonString += ",\n";
 jsonString += "\t\t\t\"intensity\": \"100\",\n";
 jsonString += "\t\t\t\"color\" : \"#384646\",\n";
 jsonString += "\t\t},\n";
 jsonString += "\t\t\"idLightOffice\" : {\n";
 jsonString += "\t\t\t\"state\" : ";
 jsonString += ledOfficeState;
 jsonString += ",\n";
 jsonString += "\t\t\t\"intensity\": \"100\",\n";
 jsonString += "\t\t\t\"color\" : \"#384646\",\n";
 jsonString += "\t\t},\n";
 jsonString += "\t\t\"idLightBedroom\" : {\n";
 jsonString += "\t\t\t\"state\" : ";
 jsonString += ledBedroomState;
 jsonString += ",\n";
 jsonString += "\t\t\t\"intensity\": \"100\",\n";
 jsonString += "\t\t\t\"color\" : \"#384646\",\n";
 jsonString += "\t\t},\n";
 jsonString += "\t\t\"idLightBathroom\" : {\n";
 jsonString += "\t\t\t\"state\" : ";
 jsonString += ledBathroomState;
 jsonString += ",\n";
 jsonString += "\t\t\t\"intensity\": \"100\",\n";
 jsonString += "\t\t\t\"color\" : \"#384646\",\n";
 jsonString += "\t\t},\n";
 jsonString += "\t\t\"idWindowSalon\" : {\n";
 jsonString += "\t\t\t\"state\" : 1,\n";
 jsonString += "\t\t},\n";
 jsonString += "\t\t\"idSmartBoxSalon\" : {\n";
 jsonString += "\t\t\t\"info1\": {\n";
 jsonString += "\t\t\t\t\"temperature\": \"27,3\",\n";
 jsonString += "\t\t\t\t\"unite\": \"°C\",\n";
 jsonString += "\t\t\t},\n";
 jsonString += "\t\t\t\"info2\": {\n";
 jsonString += "\t\t\t\t\"humidity\": \"23\",\n";
 jsonString += "\t\t\t\t\"unite\": \"%\",\n";
 jsonString += "\t\t\t}\n";
 jsonString += "\t\t}\n";
 if (compteur == 9) {
  jsonString += "\t}\n"; 
 } else {
  jsonString += "\t},\n"; 
 }
}

void generateEndJson() {
  jsonString += "}\n";
}
