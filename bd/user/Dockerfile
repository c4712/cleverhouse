FROM ubuntu

# Add the PostgreSQL PGP key to verify their Debian packages.
ARG DEBIAN_FRONTEND=noninteractive
# Install ``python-software-properties``, ``software-properties-common`` and PostgreSQL 14.1
#  There are some warnings (in red) that show up during the build. You can hide
#  them by prefixing each apt-get statement with DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y software-properties-common  postgresql-client postgresql-contrib build-essential

# Note: The official Debian and Ubuntu images automatically ``apt-get clean``
# after each ``apt-get``
RUN apt install -y python3.9
RUN apt install python3-pip -y
RUN apt install python3.8-dev -y
RUN apt-get update \
    && apt-get -y install libpq-dev gcc \
    && pip install psycopg2

COPY init.sql /docker-entrypoint-initdb.d/init.sql
COPY initTest.sql /docker-entrypoint-initdb.d/initTest.sql
COPY test.py /docker-entrypoint-initdb.d/test.py
RUN chmod a+x /docker-entrypoint-initdb.d/test.py
USER postgres


ENV POSTGRES_PASSWORD="password"
ENV POSTGRES_USER="postgres"
ENV POSTGRES_DATABASE="cleverhousebd"
ENV POSTGRES_HOST_AUTH_METHOD=trust
ENV PGDATA=/var/lib/postgresql/data/postdata

# Create a PostgreSQL role named ``docker`` with ``docker`` as the password and
# then create a database `docker` owned by the ``docker`` role.
# Note: here we use ``&&\`` to run commands one after the other - the ``\``
#       allows the RUN command to span multiple lines.


# Adjust PostgreSQL configuration so that remote connections to the
# database are possible.
RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/12/main/pg_hba.conf

# And add ``listen_addresses`` to ``/etc/postgresql/12.9/main/postgresql.conf``
RUN echo "listen_addresses='*'" >> /etc/postgresql/12/main/postgresql.conf

RUN pg_ctlcluster 12 main start
RUN pg_lsclusters
RUN etc/init.d/postgresql restart
RUN service postgresql start
RUN pg_lsclusters

# Expose the PostgreSQL port
EXPOSE 5432

CMD ["/usr/lib/postgresql/12/bin/postgres", "-D", "/var/lib/postgresql/12/main", "-c", "config_file=/etc/postgresql/12/main/postgresql.conf"]
