from typing import List


from fastapi import FastAPI
from pydantic import BaseModel

ch_api = FastAPI()


class Device(BaseModel):
    idDevice: int
    deviceName: str
