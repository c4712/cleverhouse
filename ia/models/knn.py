from sklearn.cluster import KMeans
import numpy as np
import json
from sklearn import preprocessing
from sklearn.preprocessing import OneHotEncoder
from sklearn.neighbors import NearestNeighbors

def createDictTrain():
    with open('log_full.json') as a:
        dict1 = json.load(a)
    features=[]
    label=[]
    for i in list(dict1.keys())[:-5]:
        tablo=[]
        newState=dict1[i]["deviceChanged"]["newState"]
        precState=dict1[i]["deviceChanged"]["precState"]
        date=dict1[i]["meta"]["date"]
        userChange=dict1[i]["meta"]["userChange"]
        label.append(newState)
        tablo.append(precState)
        tablo.append(date)
        tablo.append(userChange)
        for j in list(dict1[i]["system"].keys())[:-5]:
            color=dict1[i]["system"][j]["color"]
            intensity=dict1[i]["system"][j]["intensity"]
            state=dict1[i]["system"][j]["state"]
            tablo.append(color)
            tablo.append(intensity)
            tablo.append(state)
        features.append(tablo)
    return features,label

def createDictPredict():
    with open('log_full.json') as a:
        dict1 = json.load(a)
    features=[]
    for i in list(dict1.keys())[-5:]:
        tablo=[]
        precState=dict1[i]["deviceChanged"]["precState"]
        date=dict1[i]["meta"]["date"]
        userChange=dict1[i]["meta"]["userChange"]
        tablo.append(precState)
        tablo.append(date)
        tablo.append(userChange)
        for j in list(dict1[i]["system"].keys())[:-5]:
            color=dict1[i]["system"][j]["color"]
            intensity=dict1[i]["system"][j]["intensity"]
            state=dict1[i]["system"][j]["state"]
            tablo.append(color)
            tablo.append(intensity)
            tablo.append(state)
        features.append(tablo)
    return features

def encoder(X,Y):
    enc = OneHotEncoder(handle_unknown='ignore')
    enc.fit(X)
    enc.fit(Y)
    return enc.transform(X).toarray(), enc.transform(Y).toarray()

X,Y = createDictTrain()
featuresTest = createDictPredict()
X,featuresTest=encoder(X,featuresTest)
from sklearn.neighbors import KNeighborsClassifier
neigh = KNeighborsClassifier(n_neighbors=3)
neigh.fit(X, Y)
print(neigh.predict(featuresTest))
