import React, { FunctionComponent } from 'react'
import { Image, StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native'

interface Props {
    icon: string
    label: string
    selected: boolean
}
const Room: FunctionComponent<Props> = ({ icon, label, selected }) => (
    <View style={itemStyle(selected)}>
        <View style={styles.itemLeft}>
            <Image
                source={{ uri: icon }}
                resizeMode="contain"
                style={{ width: '15vw', height: '15vw', marginBottom: '0.6rem' }}
            />
            <Text style={styles.itemText}>{label}</Text>
        </View>
    </View>
)

const itemStyle = (selected: boolean): StyleProp<ViewStyle> => {
    return {
        backgroundColor: selected ? '#e91e63' : '#B4A7AF',
        borderStyle: 'solid',
        borderColor: 'black',
        borderWidth: 2,
        padding: 5,
        borderRadius: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '25vw',
        height: '25vw',
        margin: 7,
    }
}

const styles = StyleSheet.create({
    itemLeft: {
        flexDirection: 'column',
        alignItems: 'center',
        flexWrap: 'wrap',
        maxWidth: '100%',
        alignContent: 'center',
        justifyContent: 'center',
        padding: 5,
    },
    itemText: {
        maxWidth: '100%',
        color: '#000',
        fontSize: 12,
        display: 'flex',
    },
})

export default Room
