import React, { FunctionComponent } from 'react'
import { Image, StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native'

interface Props {
    icon: string
    label: string
    selected: boolean
}

const DeviceComponent: FunctionComponent<Props> = ({ icon, label, selected }) => {
    console.log({ icon })

    return (
        <View style={itemStyle(selected)}>
            <View style={styles.itemLeft}>
                <Image
                    source={{ uri: icon }}
                    resizeMode="contain"
                    style={{
                        width: '12vw',
                        height: '12vw',
                        marginHorizontal: '1rem',
                        borderRadius: 4,
                    }}
                />
                <Text style={styles.itemText}>{label}</Text>
            </View>
        </View>
    )
}

const itemStyle = (selected: boolean): StyleProp<ViewStyle> => {
    return {
        backgroundColor: selected ? '#e91e63' : '#B4A7AF',
        borderStyle: 'solid',
        borderColor: 'black',
        borderWidth: 2,
        padding: '0.5rem',
        borderRadius: 8,
        margin: 7,
        width: '80vw',
        opacity: 0.85,
    }
}

const styles = StyleSheet.create({
    item: {
        padding: '0.5rem',
        borderRadius: 8,
        backgroundColor: 'black',
        margin: 7,
        width: '80vw',
        opacity: 0.85,
    },
    itemLeft: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    itemText: {
        maxWidth: '80%',
        color: '#000',
        fontSize: 15,
    },
})

export default DeviceComponent
