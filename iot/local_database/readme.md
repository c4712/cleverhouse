# Initialisation

Pour la configuration des tables, il vous suffit de créer un shéma dans le fichier CreateTable.sql

Exemple :

```bash
CREATE TABLE devices (
id_device varchar(25),
device_name  varchar(25),
status boolean
);
```
Vous pouvez aussi initialiser des données dans la table avec le fichier InsertData.sql

# Installation

Exécuter le script.

```bash
./init_docker.sh
```

Lancer le container.

```bash
docker start my-mysql 
```

Exécuter le container.

```bash
docker exec -it my-mysql bash
```

Une fois le container exécuté, tapez cette commande.

```bash
mysql -uroot -p
```

Le mot de passe par défaut est root. Si vous souhaitez le modifier, vous pouvez le faire dans le script ./init_docker.sh

Une fois dans SQL, vous pouvez vérifier vos tables.

```bash
use company;
show tables;
select * from (nom_de_votre_table);
```

Pour infos, le nom de la database peut aussi être modifier.